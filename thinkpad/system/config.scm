(define-module (thinkpad system config)
  #:use-module (gnu)
  #:use-module (gnu services)
  #:use-module (gnu system privilege)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu system linux-initrd)
  #:use-module (gnu services configuration)
  #:use-module (thinkpad system services firewall))

(use-service-modules
 dns desktop networking ssh xorg security-token virtualization avahi dbus
 sound admin)
(use-package-modules
 security-token certs wm suckless emacs emacs-xyz libusb nfs gnome linux)

(define %zsa-udev-rule
  (udev-rule
   "50-zsa.rules"
   "# Rules for Oryx web flashing and live training
KERNEL==\"hidraw*\", ATTRS{idVendor}==\"16c0\", MODE=\"0664\", GROUP=\"plugdev\"
KERNEL==\"hidraw*\", ATTRS{idVendor}==\"3297\", MODE=\"0664\", GROUP=\"plugdev\""))

(define %nftables-conf
  (my-nftables-configuration
   (tables
    (list
     (nftables-table
      (name "filter")
      (chains
       (list
        (nftables-chain
         (name "input")
         (hook 'input)
         (policy 'drop)
         (rules
          '(;; Accept loopback
            "meta iiftype loopback accept"
            ;; Accept known connections, but drop invalid ones.
            "ct state { established, related } accept"
            "ct state invalid drop"
            ;; Accept icmp.  TODO: filtering some of them.
            "meta l4proto { icmp, ipv6-icmp } accept"
            ;; Counters.
            "counter")))
        (nftables-chain
         (name "forward")
         (hook 'forward)
         (policy 'drop))))
      (maps '())
      (sets '()))))))

(operating-system
 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))

 (locale "en_GB.utf8")
 (timezone "Europe/Paris")
 (keyboard-layout (keyboard-layout "fr"))
 (host-name "thinkpad")

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
                (name "ds-ac")
                (comment "Arnaud Carmajit Daby-Seesaram")
                (group "users")
                (home-directory "/home/ds-ac")
                (supplementary-groups '("wheel" "netdev" "audio" "video"
                                        "plugdev")))
               %base-user-accounts))

 (packages %base-packages)

 (services
  (append (list

           ;; Udev: ZSA rules.
           (udev-rules-service 'usb-thing %zsa-udev-rule)

           ;; Networking.
           (service nftables-service-type
                    (nftables-configuration
                     (ruleset
                      (plain-file
                       "nftables.conf"
                       (serialize-my-nftables-configuration
                        %nftables-conf)))))

           (service network-manager-service-type
                    (network-manager-configuration
                     (dns "dnsmasq")))
           (service wpa-supplicant-service-type)

           (simple-service 'nm-setuid-helpers privileged-program-service-type
                           (map (lambda (program)
                                  (privileged-program
                                   (program
                                    (file-append network-manager program))
                                   (setuid? #t)))
                                '("/bin/nmtui" "/bin/nmcli")))

           ;; Other architectures.
           (service qemu-binfmt-service-type
                    (qemu-binfmt-configuration
                     (platforms (lookup-qemu-platforms
                                 "i386" "riscv32" "riscv64" "arm" "aarch64"))))

           ;; Yubikey-related.
	   (service pcscd-service-type)
           (udev-rules-service 'fido2 libfido2 #:groups '("plugdev"))
           (udev-rules-service 'yubikey yubikey-personalization)

           ;; SSH.
           (service openssh-service-type
                    (openssh-configuration
                     (x11-forwarding? #f)
                     (permit-root-login 'prohibit-password)))

           ;; Add udev rules for scanners.
           ;; (service sane-service-type)

           ;; Add polkit rules, so that non-root users in the wheel group can
           ;; perform administrative tasks (similar to "sudo").
           polkit-wheel-service

           (service screen-locker-service-type
                    (screen-locker-configuration
                     (name "swaylock")
                     (program (file-append swaylock "/bin/swaylock"))
                     (using-pam? #t)
                     (using-setuid? #f)))

           ;; Allow desktop users to also mount NTFS and NFS file systems
           ;; without root.

           (simple-service
            'mount-setuid-helpers privileged-program-service-type
            (map (lambda (program)
                   (privileged-program
                    (program program)
                    (setuid? #t)))
                 (list (file-append nfs-utils "/sbin/mount.nfs")
                       (file-append ntfs-3g "/sbin/mount.ntfs-3g"))))

           ;; The global fontconfig cache directory can sometimes contain
           ;; stale entries, possibly referencing fonts that have been GC'd,
           ;; so mount it read-only.
           fontconfig-file-system-service

           (service usb-modeswitch-service-type)

           ;; The D-Bus clique.
           (service upower-service-type)
           (service accountsservice-service-type)
           (service colord-service-type)
           (service polkit-service-type)
           (service elogind-service-type)
           (service dbus-root-service-type)

           (service ntp-service-type))

          (modify-services %base-services
             (guix-service-type config => (guix-configuration
               (inherit config)
               (substitute-urls
                (cons* "https://substitutes.nonguix.org"
                       "https://guix.bordeaux.inria.fr"
                       %default-substitute-urls))
               (authorized-keys
                (cons*
                 (local-file "./aux-files/science.pub")
                 (local-file "./aux-files/non-guix.pub")
                 %default-authorized-guix-keys)))))))

 (sudoers-file
  (plain-file "sudoers" "\
root ALL=(ALL) ALL\n\
%wheel ALL=(ALL) ALL\n"))

 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))
 (mapped-devices (list (mapped-device
                        (source (uuid
                                 "ddfd21bd-94eb-412b-85e4-8fbd7354cef6"))
                        (target "cryptroot")
                        (type luks-device-mapping))))

 (file-systems (cons* (file-system
                       (mount-point "/boot/efi")
                       (device (uuid "2052-ACD3"
                                     'fat32))
                       (type "vfat"))
                      (file-system
                       (mount-point "/tmp")
                       (device "none")
                       (type "tmpfs")
                       (options "size=1G"))
                      (file-system
                       (mount-point "/")
                       (device "/dev/mapper/cryptroot")
                       (type "ext4")
                       (dependencies mapped-devices))
                      %base-file-systems)))
