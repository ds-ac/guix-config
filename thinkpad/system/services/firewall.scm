(define-module (thinkpad system services firewall)
  #:use-module (gnu)
  #:use-module (guix)
  #:use-module (guix repl)
  #:use-module (guix records)
  #:use-module (gnu services configuration)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (my-nftables-configuration
            nftables-table nftables-chain
            nftables-set nftables-map
            serialize-my-nftables-configuration))

;;;;;;;;;;;;;;;;;;;;
;; Miscellaneous. ;;
;;;;;;;;;;;;;;;;;;;;

(define (serialize-list-of serializer)
  (lambda (lt)
    (fold-right
     (lambda (a acc) (string-append (serializer a) acc))
     ""
     lt)))



;;;;;;;;;;;;
;; Rules. ;;
;;;;;;;;;;;;

(define (serialize-rule r)
  (string-append "\t\t" r "\n"))

(define (list-of-rules? l) (every string? l))
(define serialize-rules
  (serialize-list-of serialize-rule))



;;;;;;;;;;;;;
;; Chains. ;;
;;;;;;;;;;;;;

(define (chain-type? t)
  (member t '(filter route nat)))

(define (chain-priority? h)
  (member h '(raw mangle dstnat filter security srcnat out)))

(define (chain-policy? p)
  (member p '(accept drop)))

(define (chain-hook? h)
  (member h '(prerouting input forward output postrouting ingress egress)))

(define-configuration/no-serialization nftables-chain
  (name string "Name of the chain")
  (type (chain-type 'filter) "Type of the chain")
  (hook chain-hook "Hook of the chain")
  (priority (chain-priority 'filter) "Priority of the chain")
  (policy (chain-policy 'accept) "Policy (if any) of the chain")
  (rules (list-of-rules '()) "List of rules"))

(define (list-of-chains? l) (every nftables-chain? l))

(define (serialize-chain c)
  (let ((name (nftables-chain-name c))
        (type (nftables-chain-type c))
        (hook (nftables-chain-hook c))
        (priority (nftables-chain-priority c))
        (policy (nftables-chain-policy c))
        (rules (nftables-chain-rules c)))
    (string-append
     "\tchain " name " {\n"

     "\t\ttype " (symbol->string type)
     " hook " (symbol->string hook)
     " priority " (symbol->string priority) ";"
     " policy " (symbol->string policy) ";\n"

     (serialize-rules rules)

     "\t}\n")))

(define serialize-chains
  (serialize-list-of serialize-chain))


;;;;;;;;;;;
;; Sets. ;;
;;;;;;;;;;;

(define-configuration/no-serialization nftables-set
  (name string "."))

(define (list-of-sets? l) (every nftables-set? l))

(define (serialize-set s)
  (let ((name (nftables-set-name s)))
    (string-append
     "\tset " name " {\n"
     ;; TODO.
     "\t}\n")))

(define serialize-sets
  (serialize-list-of serialize-set))



;;;;;;;;;;;
;; Maps. ;;
;;;;;;;;;;;

(define-configuration/no-serialization nftables-map
  (name string "."))

(define (list-of-maps? l) (every nftables-map? l))

(define (serialize-map s)
  (let ((name (nftables-map-name s)))
    (string-append
     "\tmap " name " {\n"
     ;; TODO.
     "\t}\n")))

(define serialize-maps
  (serialize-list-of serialize-map))



;;;;;;;;;;;;;
;; Tables. ;;
;;;;;;;;;;;;;

(define (addr-type? a)
  (member a '(ip ip6 inet arp bridge netdev)))

(define-configuration/no-serialization nftables-table
  (name string "Name of the table")
  (type (addr-type 'inet) "Address type")
  (sets (list-of-sets '()) "List of sets of the table")
  (maps (list-of-maps '()) "List of maps of the table")
  (chains list-of-chains "List of chains of the table."))

(define (serialize-nftables-table t)
  (let ((type (nftables-table-type t))
        (name (nftables-table-name t))
        (sets (nftables-table-sets t))
        (maps (nftables-table-maps t))
        (chains (nftables-table-chains t)))
    (string-append
     "table " (symbol->string type) " " name " {\n"
     (serialize-sets sets)
     (serialize-maps maps)
     (serialize-chains chains)
     "}\n")))

(define (list-of-tables? t) (every nftables-table? t))

(define serialize-list-of-tables
  (serialize-list-of serialize-nftables-table))



;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Whole configuration. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-configuration/no-serialization my-nftables-configuration
  (tables
   list-of-tables
   "List of nftables tables."))

(define (serialize-my-nftables-configuration conf)
  (let ((tables (my-nftables-configuration-tables conf)))
    (string-append
     "# Add smth like 'nft -f'?\n\nflush ruleset\n\n"
     (serialize-list-of-tables tables))))
