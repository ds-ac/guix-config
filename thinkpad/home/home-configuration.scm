(define-module (thinkpad home home-configuration)
  #:use-module (thinkpad home aux-files secrets)
  #:use-module (gnu home services sway)
  #:use-module (gnu)
  #:use-module (gnu home)
  #:use-module (gnu packages)
  #:use-module (gnu home services)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (gnu home services pm)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services dotfiles)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services xdg)
  #:use-module (gnu home services guix)
  #:use-module (gnu home services sound)
  #:use-module (gnu home services mcron)
  #:use-module (srfi srfi-1))

(use-package-modules
 scheme virtualization markup gnu-doc
 texinfo base commencement compression freedesktop package-management
 rsync coq kde glib tmux screen admin rust-apps suckless wm video image-viewers
 gimp linux curl wget web web-browsers tor-browsers gnuzilla man mail gnupg
 aspell hunspell package-management emacs emacs-xyz terminals shells
 shellutils imagemagick ocaml ssh gnome xdisorg image pdf guile
 guile-xyz dictionaries backup version-control pulseaudio password-utils cups)


;;;
;;; Sway configuration.
;;;

(define (sway-conf)
  "This procedure returns a @code{sway-configuration} record.

@quotation Note
My background files are computed as @file{.png} files, because I do not make
@code{librsvg} available in my @code{$PATH}.
@end quotation"

  (define exec-emacsclient
    #~(string-append
       "exec " #$emacs "/bin/emacsclient -c -a '"
       #$libnotify "/bin/notify-send Oups \"emacs not loaded :/\"'"))

  (define (brightness-change i)
    "Adds i% to the current brightness of the built-in output of my laptop."
    (let ((change (if (< i 0)
                      (string-append (number->string (* -1 i)) "%-")
                      (string-append "+" (number->string i) "%"))))
      #~(string-append
         "exec " #$(file-append brightnessctl "/bin/brightnessctl")
         " -q s " #$change)))

  (define (guix-background name)
    "This procedure computes @file{NAME.png} from @file{NAME.svg}.  Note that
@file{NAME.svg} must exists in the @file{backgrounds/guix} directory provided by
@code{guix-backgrounds}."
    (computed-file
     (string-append name ".png")
     #~(begin
         (let* ((insvg (string-append
                        #$guix-backgrounds
                        "/share/backgrounds/guix/" #$name ".svg"))
                (out #$output)
                (cmd
                 (string-append
                  #$librsvg "/bin/rsvg-convert " insvg " -o " out)))
           (system cmd)))))

  (define sway-status-command
    ;; (with-extensions (list mu)
    (program-file
     "sway-bar-status.scm"
     #~(begin
         (use-modules (ice-9 textual-ports)
                      ;; (mu)
                      (ice-9 format)
                      (ice-9 popen)
                      (ice-9 regex)
                      (srfi srfi-19))
         ;; (mu:initialize)
         ;; (define (count-emails request)
         ;;   (length (mu:message-list request)))
         ;; (define (mu-unread-ratio mailbox)
         ;;   (let* ((request-total (string-append "maildir:" mailbox))
         ;;          (request-unread
         ;;            (string-append request-total " flags:unread"))
         ;;          (count-read (count-emails request-total))
         ;;          (count-unread (count-emails request-unread)))
         ;;     (format #f "~a/~a" count-unread count-read)))
         (let loop ()
           (let* (;; Email-statistics
                  ;; (mail-nanein (mu-unread-ratio "/nanein/INBOX"))

                  ;; Date
                  (date
                   (date->string (current-date)
                                 "~a ~b ~d ~H:~M:~S ~Y (W ~W)"))

                  ;; Battery-information
                  (batline
                   (let* ((p (open-pipe*
                              OPEN_READ
                              #$(file-append acpi "/bin/acpi") "-b"))
                          (bat (get-line p)))
                     (close-pipe p)
                     bat))
                  (bat
                   (match:substring (string-match "[0-9]+%" batline))))
             (format #t "~a - ~a~%~!"  bat date)
             (sleep 1)
             (loop))))))

  (sway-configuration
   (outputs
    (list
     (sway-output (identifier "eDP-1")
                  ;; Built-in screen.
                  (background (guix-background "guix-checkered-16-9"))
                  (position
                   (point (x 0)
                          (y 0))))
     (sway-output (identifier "Synaptics Inc Non-PnP 0x01010101")
                  ;; Office screen (rightmost screen).
                  (background (guix-background "guix-checkered-16-9"))
                  (resolution "1920x1080")
                  (position
                   (point (x 1366) ;; eDP-1 is 1366-wide
                          (y 0))))
     (sway-output (identifier "HP Inc. HP E24i G4 6CM12303XK")
                  ;; Office screen (middle screen).
                  (background (guix-background "guix-checkered-16-9"))
                  (position
                   (point (x (+ 1920 1366))
                          (y 0))))))
   (inputs
    (list
     (sway-input (identifier "type:keyboard")
                 (layout
                  (keyboard-layout "fr" #:options '("ctrl:nocaps"))))
     (sway-input
      (identifier "type:touchpad")
      (tap #t)
      (disable-while-typing #t)
      (disable-while-trackpointing #t))))
   (bar (sway-bar
         (status-command sway-status-command)
         (position 'top)
         (extra-content '("mode hide"))
         (mouse-bindings `((,%ev-code-mouse-left . "exec $term")
                           (,%ev-code-mouse-right . ,exec-emacsclient)
                           (,%ev-code-mouse-scroll-click . "kill")))
         (colors (sway-color
                  (statusline "#ffffff")
                  (background "#000000")
                  (inactive-workspace (sway-border-color
                                       (border "#32323200")
                                       (background "#32323200")
                                       (text "#5c5c5c")))))))
   (extra-content
    (list
     ;; After making sure that everything works properly: "xwayland disable"
     "xwayland enable"
     "default_border none"))
   (keybindings
    `(($mod+Shift+e . ,exec-emacsclient)
      (XF86HomePage . ,(brightness-change -1))
      (XF86Favorites . ,(brightness-change 1))
      (XF86MonBrightnessDown . ,(brightness-change -1))
      (XF86MonBrightnessUp . ,(brightness-change 1))
      ($mod+Return . "exec $term")
      ($mod+Shift+c . "kill")
      ($mod+p . "exec $menu")
      ($mod+Shift+r . "reload")
      ($mod+Shift+q
       . ,#~(string-append
             "exec " #$sway "/bin/swaynag -t warning -m \\\n    "
             "'You pressed the exit shortcut.  Do you really want to exit sway?"
             " This will end your Wayland session.' \\\n    "
             "-B 'Yes, exit sway' \\\n    '"
             #$sway "/bin/swaymsg exit'"))
      ($mod+$left . "focus left")
      ($mod+$right . "focus right")
      ($mod+$up . "focus up")
      ($mod+$down . "focus down")
      ($mod+Shift+$left . "move left")
      ($mod+Shift+$right . "move right")
      ($mod+Shift+$up . "move up")
      ($mod+Shift+$down . "move down")
      ($mod+ampersand . "workspace number 1")
      ($mod+eacute . "workspace number 2")
      ($mod+quotedbl . "workspace number 3")
      ($mod+apostrophe . "workspace number 4")
      ($mod+parenleft . "workspace number 5")
      ($mod+minus . "workspace number 6")
      ($mod+egrave . "workspace number 7")
      ($mod+underscore . "workspace number 8")
      ($mod+ccedilla . "workspace number 9")
      ($mod+agrave . "workspace number 10")
      ($mod+Shift+ampersand . "move container to workspace number 1")
      ($mod+Shift+eacute . "move container to workspace number 2")
      ($mod+Shift+quotedbl . "move container to workspace number 3")
      ($mod+Shift+apostrophe . "move container to workspace number 4")
      ($mod+Shift+parenleft . "move container to workspace number 5")
      ($mod+Shift+minus . "move container to workspace number 6")
      ($mod+Shift+egrave . "move container to workspace number 7")
      ($mod+Shift+underscore . "move container to workspace number 8")
      ($mod+Shift+ccedilla . "move container to workspace number 9")
      ($mod+Shift+agrave . "move container to workspace number 10")
      ($mod+b . "splith")
      ($mod+v . "splitv")
      ($mod+f . "fullscreen")
      ($mod+Shift+space . "floating toggle")
      ($mod+space . "focus mode_toggle")
      ($mod+z . "focus parent")
      ($mod+Shift+o . "move scratchpad")
      ($mod+o . "scratchpad show")
      ($mod+r . "mode \"resize\"")
      ($mod+Shift+n . "bar mode toggle")
      ($mod+Shift+b . "border toggle")
      ($mod+tab . "workspace back_and_forth")))
   (gestures
    (append
     `((swipe:4:up . ,exec-emacsclient))
     %sway-default-gestures))
   (packages
    (list
     dbus sway slurp grim xdg-desktop-portal xdg-desktop-portal-wlr))
   (startup+reload-programs
    (list
     #~(string-append #$dbus "/bin/dbus-update-activation-environment "
                      "--systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway")
     "$HOME/.guix-home/profile/libexec/xdg-desktop-portal -r"
     "$HOME/.guix-home/profile/libexec/xdg-desktop-portal-wlr -r"))
   (startup-programs
    (list
     #~(string-append #$dunst "/bin/dunst")
     #~(string-append #$emacs "/bin/emacs --bg-daemon")
     #~(string-append
        #$swayidle "/bin/swayidle -w \\\n    "
        ;; 300: lock screen.
        "timeout 300 '" #$swaylock "/bin/swaylock "
        "--indicator-radius 75 \\\n    "
        "-i " #$(guix-background "guix-encircled-checkered-16-9") " \\\n    "
        "-f -c 000000' \\\n    "
        ;; 600: lock + screen off.
        "timeout 600 '" #$sway "/bin/swaymsg \"output * power off\"' \\\n    "
        ;; Resume + sleep.
        "resume '" #$sway "/bin/swaymsg \"output * power on\"' \\\n    "
        "before-sleep '" #$swaylock "/bin/swaylock -f -c 000000'")))))


;;;
;;; E-mail configuration.
;;;

(define (isync-passcmd name)
  (string-append
   "gpg --decrypt --for-your-eyes-only $HOME/.password-store/perso/"
   name ".gpg 2>/dev/null"))

(define-configuration/no-serialization
  isync-account
  (name
   (string)
   "Account name.")
  (port
   (integer 993)
   "Port to use.")
  (host
   (string)
   "Hostname of the IMAP server.")
  (user
   (string)
   "User.")
  (passcmd
   (string)
   "Password command to execute to retrieve the password."))

(define (serialize-isync-account acc)
  (let ((name (isync-account-name acc))
        (host (isync-account-host acc))
        (port (number->string (isync-account-port acc)))
        (pass (isync-account-passcmd acc))
        (user (isync-account-user acc))
        (make-line (lambda (l)
                     (string-append (car l) " " (car (cdr l)) "\n"))))
    (string-concatenate
     (map make-line
          (list
           `("# Account:" ,name)
           `("IMAPAccount" ,name)
           `("Host" ,host)
           `("Port" ,port)
           `("User" ,user)
           `("PassCmd" ,(string-append "\""  pass "\""))
           '("TLSType" "IMAPS")
           '("CertificateFile" "/etc/ssl/certs/ca-certificates.crt\n")
           `("IMAPStore" ,(string-append name "-remote"))
           `("Account" ,(string-append name "\n"))
           `("MaildirStore" ,(string-append name "-local"))
           '("SubFolders" "Verbatim")
           `("Path" ,(string-append "~/.mail/" name "/"))
           `("Inbox" ,(string-append "~/.mail/" name "/INBOX\n"))
           `("Channel" ,name)
           `("Far" ,(string-append ":" name "-remote:"))
           `("Near" ,(string-append ":" name "-local:"))
           '("Patterns" "*")
           '("Create" "Both")
           '("Expunge" "Both")
           '("SyncState" "*\n\n"))))))

(define (serialize-isync-accounts accs)
  (string-concatenate (map serialize-isync-account accs)))

(define (isync-accounts->file configurations)
  (plain-file "mbsyncrc"
              (serialize-isync-accounts configurations)))

(define (home-isync-service-type isync-accounts)
  (simple-service
   'home-isync
   home-files-service-type
   `((".mbsyncrc" ,(isync-accounts->file isync-accounts)))))

(define %isync-accounts
  (list
   (isync-account
    (name "nanein")
    (host "mx1.nanein.fr")
    (user "ds-ac")
    (passcmd (isync-passcmd "nanein_dsac_mdp")))
   (isync-account
    (name "crans")
    (host "owl.crans.org")
    (user "ds-ac")
    (passcmd (isync-passcmd "crans_mdp")))
   (isync-account
    (name "free")
    (host "imap.free.fr")
    (user "arnaud.daby@free.fr")
    (passcmd (isync-passcmd "zimbra_mdp")))
   (isync-account
    (name "ENS")
    (host "imap.ens-paris-saclay.fr")
    (user "adabys01")
    (passcmd (isync-passcmd "ENS_mdp")))
   (isync-account
    (name "ionos")
    (host "imap.ionos.fr")
    (user "arnaud@dabyseesaram.info")
    (passcmd (isync-passcmd "ionos_mdp")))))


;;;
;;; Main configuration.
;;;

(home-environment
 (packages
  (list
   ;; Emacs-related packages.
   dash emacs emacs-undo-tree emacs-which-key emacs-hydra emacs-yasnippet
   emacs-flycheck
   emacs-org-tree-slide emacs-markdown-mode emacs-paredit
   emacs-emms

   emacs-lsp-mode emacs-lsp-ui

   ;; Version control with Git (I do not use any other VC)
   git (list git "send-email") emacs-magit

   ;; Password-store
   emacs-pass
   password-store

   ;; Environment management.
   direnv emacs-direnv
   ;; install dune here iff I want the Emacs packages to be installed
   ;; profile-wide.

   ;; Music- and video-related
   mpv

   ;; Some basic texlive stuff.
   emacs-auctex emacs-cdlatex emacs-helm-bibtex emacs-company-math

   ;; Terminal- and terminal-emulator-related packages.
   tmux emacs-vterm

   ;; Common terminal utilities
   htop ripgrep tree bat pulsemixer imagemagick translate-shell

   ;; Network- and Web-related
   ;; nss-certs is installed system-wide
   qutebrowser curl wget

   ;; Misc.
   rsync man-pages-posix man-db man-pages borg feh openssh
   gnu-standards

   ;; Info documents.
   sicp

   ;; Notifications.
   libnotify

   ;; PDFs- and TeX-related.
   mupdf pdfgrep emacs-pdf-tools

   ;; Guile.
   guile-3.0 guile-colorized guile-readline emacs-geiser emacs-geiser-guile

   ;; Emails and GPG.
   mu isync offlineimap3 gmime gnupg pinentry-qt
   emacs-message-view-patch

   ;; Spelling.
   ispell hunspell-dict-en-gb hunspell-dict-fr-classique aspell aspell-dict-fr
   aspell-dict-en emacs-flyspell-correct emacs-ac-ispell

   ;; OCaml development
   emacs-tuareg

   ;; Coq-related packages.
   emacs-company-coq
   proof-general))

 (services
  (cons*
   ;; Dotfiles
   (service home-dotfiles-service-type
            (home-dotfiles-configuration
             (directories '("./aux-files/dotfiles"))))

   ;; Sway configuration.
   (service home-sway-service-type (sway-conf))

   ;; isync configuration.
   (home-isync-service-type %isync-accounts)

   ;; This services provides `pipewire' with pulseaudio compatibility.
   (service home-pipewire-service-type
            (home-pipewire-configuration
             (enable-pulseaudio? #t)))

   ;; This service provides a running dbus session.
   (service home-dbus-service-type)

   ;; `batsignal` watches the state of the battery and emits a warning when it
   ;; is low.
   (service home-batsignal-service-type)

   (simple-service 'more-info-manuals
                   home-environment-variables-service-type
                   `(("MYINFOPATH"
                      . ,#~(string-join
                            (map
                             (lambda (e) (string-append e "/share/info"))
                             (list #$gcc-toolchain #$haunt #$guile-g-golf))
                            ":" 'suffix))))

   ;; Remember to make backups regularly
   (service home-mcron-service-type
            (home-mcron-configuration
             (jobs (list
                    #~(job "0 22 * * *"
                           #$(program-file
                              "dodo.scm"
                              #~(begin
                                  (system*
                                   #$(file-append libnotify "/bin/notify-send")
                                   "Psst"
                                   "Il est temps d'aller se coucher!"))))
                    #~(job "0 15 * * Fri"
                           #$(program-file
                              "reminder-backups.scm"
                              #~(begin
                                  (system*
                                   #$(file-append libnotify "/bin/notify-send")
                                   "Psst"
                                   "Il est temps de faire une backup!"))))))))

   ;; `home-bash-service` builds and installs bash-configuration files.
   (service home-bash-service-type
            (home-bash-configuration
             (aliases '(("grep" . "grep --color=auto")
                        ("ip" . "ip -color=auto")
                        ("gits" . "git status")
                        ("gitl" . "git log --graph")
                        ("gitla" . "gitl --all ")
                        ("ll" . "ls -l")
                        ("clear" . "echo -n -e \"\\033[2J\\033[H\"")
                        ("ls" . "ls -p --color=auto")))
             (bashrc (list (local-file "aux-files/.bashrc" "bashrc")))
             (bash-profile (list (local-file "aux-files/.bash_profile"
                                             "bash_profile")))))

   %base-home-services)))

;; Local Variables:
;; eval: (whitespace-mode 1)
;; End:
