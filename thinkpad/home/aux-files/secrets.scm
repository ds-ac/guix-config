(define-module (thinkpad home aux-files secrets)
  #:use-module (thinkpad home services mails)
  #:use-module (gnu home services ssh)
  #:export (%isync-accounts))

;;;;;;;;;;;;;;;;;;;;;;;;
;; SSH configuration. ;;
;;;;;;;;;;;;;;;;;;;;;;;;

(define %my-ssh-machines '())



;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Isync configuration. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(define %isync-accounts '())
