if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]] ; then
    echo dbus-run-session sway
fi

eval "$(direnv hook bash)"
export EDITOR='emacs -nw -Q'
export INFOPATH=$INFOPATH:$MYINFOPATH
