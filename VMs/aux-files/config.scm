(define-module (VMs aux-files config)
  #:use-module (gnu services certbot)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (gnu)
  #:export (%nginx-additional-content %certbot-configuration %nginx-101
            %default-user))

(define %default-user
  (user-account
   (name "...")
   (comment "...")
   (group "users")
   (supplementary-groups '("wheel" "audio" "video"))))



(define %nginx-101
  (machine-ssh-configuration
   (host-name "...")
   (user "...")
   (host-key "...")
   (system "x86_64-linux")))

;; From Guix manual.
;; TODO: use 111's Knot-based deploy scipt.
(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(define %certbot-configuration
  (certbot-configuration
   (email "...")
   (certificates
    (list '()))))

(define %nginx-additional-content "")
