(define-module (VMs nginx-101 configuration)
  #:use-module (VMs aux-files config)
  #:use-module (thinkpad system services firewall)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (gnu)
  #:use-module (guix)
  #:use-module (guix modules)
  #:use-module (guix gexp)
  #:export (%nginx-machine))

(use-service-modules networking ssh web certbot rsync)
(use-package-modules screen ssh certs tmux vim dns tls rsync gawk)




;;;;;;;;;;;;;;;
;; Firewall. ;;
;;;;;;;;;;;;;;;

(define %nftables-conf
  (my-nftables-configuration
   (tables
    (list
     (nftables-table
      (name "filter")
      (chains
       (list
        (nftables-chain
         (name "input")
         (hook 'input)
         (policy 'drop)
         (rules
          '(;; Accept loopback
            "meta iiftype loopback accept"

            ;; Accept known connections, but drop invalid ones.
            "ct state { established, related } accept"
            "ct state invalid drop"

            ;; Accept new connections to Web- and SSH-related ports, and rsync
            ;; connections from other VMs.
            "ct state new th dport { 80, 443, 22 } accept"
            "ct state new ip saddr 192.168.51.0/24 th dport 873 accept"

            ;; Accept icmp.  TODO: filtering some of them.
            "meta l4proto { icmp, ipv6-icmp } accept"

            ;; Counters.
            "counter")))

        (nftables-chain
         ;; Forbid forwarding.
         (name "forward")
         (hook 'forward)
         (policy 'drop))))
      (maps '())
      (sets '()))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Certbot deployment hooks. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define %nginx-deploy-hook
  ;; This hook has been copied from the Guix manual.
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(define %certbot-knot-authentication
  (program-file
   "knot-authentication"
   (with-imported-modules
       (source-module-closure
        '((ice-9 popen) (ice-9 format)))
     #~(begin
         (use-modules (ice-9 popen) (ice-9 format))
         (let* ((domain     (getenv "CERTBOT_DOMAIN"))
                (validation (getenv "CERTBOT_VALIDATION"))
                (knsupdate (string-append #$knot:tools "/bin/knsupdate"))
                (port (open-pipe* OPEN_WRITE knsupdate)))
           (format #t "Add challenge for ~a: ~s~%" domain validation)
           (format port "server 192.168.51.104~%")
           (format port "zone nanein.fr~%")
           (format port "add _acme-challenge.~a. 60 TXT ~s~%" domain validation)
           (format port "send~%~!")
           (close-pipe port)
           (sleep 1))))))

(define %certbot-knot-cleanup
  (program-file
   "knot-cleanup"
   (with-imported-modules
       (source-module-closure
        '((ice-9 popen) (ice-9 format)))
     #~(begin
         (use-modules (ice-9 popen) (ice-9 format))
         (let* ((domain     (getenv "CERTBOT_DOMAIN"))
                (validation (getenv "CERTBOT_VALIDATION"))
                (knsupdate (string-append #$knot:tools "/bin/knsupdate"))
                (port (open-pipe* OPEN_WRITE knsupdate)))
           (format #t "Removing the challenge to ~a:~%~t~a~%" domain validation)
           (format port "server 192.168.51.104~%")
           (format port "zone nanein.fr.~%")
           (format port "del _acme-challenge.~a. 60 TXT ~s~%"
                   ;; Note: While I put the validation token between quotes.
                   ;; ````` I do not think that this is necessary.
                   domain validation)
           (format port "send~%~!")
           (close-port port))))))

;; %certbot-knot-deploy is a deployment script used by Certbot.  It will:
;; -> add TLSA fields to the DNS zone (older fields are not deleted to take into
;;    account propagation delay ;
;; -> copy certificates to `/srv/rsyncd/certs', where other VMs (only yodol for
;;    now) will collect them.
(define %certbot-knot-deploy
  (program-file
   "knot-deploy"
   (with-imported-modules
       (source-module-closure
        '((ice-9 popen) (ice-9 textual-ports) (ice-9 match) (srfi srfi-1)))
     #~(begin
         (use-modules (ice-9 popen) (ice-9 textual-ports) (ice-9 match)
                      (srfi srfi-1) (ice-9 format))

         (define (install-tlsa-field hash port name)
           (let* ((port (number->string port))
                  (knsupdate (string-append #$knot:tools "/bin/knsupdate"))
                  (port (open-pipe* OPEN_WRITE knsupdate)))
             (format #t "Adding TLSA field:~%~t~a ;~%~t~a ;~%~t~s.~%"
                     name port hash)
             (format port "server 192.168.51.104~%")
             (format port "zone nanein.fr.~%")
             ;; (format port "del _~a._tcp.~a.~%" port name)
             (format port "add _~a._tcp.~a. 10 TLSA 3 0 1 ~a~%"
                     ;; Note: the hash is *not* between quotes here, as knsupdate
                     ;; ````` does not like it.
                     port name hash)
             (format port "send~%~!")
             (close-port port)))

         (define (renewed->alist str)
           (fold
            (lambda (elt acc)
              (if (string-contains elt "mx")
                  (cons* `(,elt . 25)
                         `(,elt . 587)
                         `(,elt . 465)
                         `(,elt . 993)
                         acc)
                  (cons `(,elt . 443) acc)))
            '()
            (string-split str #\ )))

         (let* ((lineage (getenv "RENEWED_LINEAGE"))
                (renewed (getenv "RENEWED_DOMAINS"))
                (keypath (string-append lineage "/privkey.pem"))
                (certpath (string-append lineage "/fullchain.pem"))
                (renewed (renewed->alist renewed))
                (p (open-pipe
                    (string-append
                     #$openssl:out "/bin/openssl x509 -in " certpath
                     " -outform DER | "
                     #$openssl:out "/bin/openssl sha256 | "
                     #$gawk "/bin/awk '{ print $2 }'")
                    OPEN_READ))
                (hash (get-line p)))
           (close-pipe p)
           (for-each
            (lambda (elt)
              (match elt
                ((name . port)
                 (let ((certdest
                        (string-append "/srv/rsyncd/certs/" name "-cert.pem"))
                       (keydest
                        (string-append "/srv/rsyncd/certs/" name "-key.pem")))
                   (install-tlsa-field hash port name)
                   (copy-file certpath certdest)
                   (copy-file keypath keydest)))
                (e
                 (format #t "unknown: ~s~%" e))))
            renewed))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Definition of the OS. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define %system
  (operating-system
   (host-name "101-nginx")
   (timezone "Europe/Paris")
   (locale "en_GB.utf8")
   (keyboard-layout (keyboard-layout "fr"))
   (initrd-modules (append (list "virtio_scsi")
                           %base-initrd-modules))
   (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (targets '("/dev/sda"))))
   (kernel-arguments (list "console=ttyS0,115200"))

   (file-systems (cons*
                  (file-system
                   (device (file-system-label "my-boot"))
                   (mount-point "/boot")
                   (type "ext2"))
                  (file-system
                   (device (file-system-label "my-root"))
                   (mount-point "/")
                   (type "ext4"))
                  %base-file-systems))

   (users (cons %default-user %base-user-accounts))

   (sudoers-file
    (plain-file "sudoers" "\
ds-ac ALL=(ALL) NOPASSWD: ALL\n"))

   ;; Globally-installed packages (nss-certs is now in %base-pacakages).
   (packages (cons* vim tmux screen rsync %base-packages))

   (services
    (cons*

     ;; SSH server.
     (service openssh-service-type
	      (openssh-configuration
               (password-authentication? #f)
	       (openssh openssh-sans-x)
	       (port-number 22)))

     ;; Auto-renewal for certificates using Certbot.
     (service
      certbot-service-type
      (certbot-configuration
       (email "ds-ac@nanein.fr")
       (certificates
        (list
         (certificate-configuration
          (domains '("nanein.fr" "mx1.nanein.fr" "mx2.nanein.fr"))
          (challenge "dns")
          (authentication-hook %certbot-knot-authentication)
          (cleanup-hook %certbot-knot-cleanup)
          (deploy-hook %certbot-knot-deploy))
         (certificate-configuration
          (domains %certbot-additional-domains)
          (deploy-hook %nginx-deploy-hook))))))

     ;; An rsync module is configured so that other allowed machines can fetch
     ;; their certificates.
     (service rsync-service-type
              (rsync-configuration
               (address "192.168.51.101")
               (modules
                (list
                 (rsync-module
                  (name "Certificates")
                  (file-name "/srv/rsyncd/certs")
                  (comment "Directory containing shared certs between VMs.")
                  (read-only? #t)
                  (chroot? #t))))))

     ;; Nginx service.  It sets up my domains.  An additional configuration
     ;; snippet is provided in `VMs/aux-files/config.scm`.
     (service
      nginx-service-type
      (nginx-configuration
       (server-blocks
        (list

         (nginx-server-configuration
          (listen '("*:80"
                    "[::]:80"))
          (server-name '("nanein.fr"))
          (raw-content
           '("if ($host = nanein.fr) {
    return 301 https://$host$request_uri;
}
return 501 \"Not implemented\";
"
             )))

         (nginx-server-configuration
          (listen '("[::]:443 ssl http2"
                    "*:443 ssl http2"))
          (server-name '("nanein.fr"))
          (root "/srv/http/nanein.fr/blog")
          (locations
           (list (nginx-location-configuration
                  (uri "/ARPE")
                  (body '("alias /srv/http/nanein.fr/ARPE;")))))
          (ssl-certificate
           "/etc/letsencrypt/live/nanein.fr/fullchain.pem")
          (ssl-certificate-key
           "/etc/letsencrypt/live/nanein.fr/privkey.pem"))))

       (extra-content %nginx-additional-content)))


     ;; Networking: defines the static IP address of the machine, and a firewall
     ;; to limit access to the aforementioned "Certificates" rsync module.
     (service nftables-service-type
                    (nftables-configuration
                     (ruleset
                      (plain-file
                       "nftables.conf"
                       (serialize-my-nftables-configuration
                        %nftables-conf)))))

     (service
      static-networking-service-type
      (list (static-networking
             (addresses
              (list
               (network-address
                (device "eth0")
                (value "2a0c:700:12:50:1::101/80"))
               (network-address
                (device "eth0")
                (value "192.168.51.101/24"))))
             (routes
              (list
               (network-route
                (destination "default")
                (gateway "2a0c:700:12:50:1::1"))
               (network-route
                (destination "default")
                (gateway "192.168.51.1"))))
             (name-servers '("192.168.51.102")))))

     (modify-services
      %base-services
      (guix-service-type
       config => (guix-configuration
                  (inherit config)
                  (authorized-keys
                   (append (list (local-file "../aux-files/signing-key.pub"))
                           %default-authorized-guix-keys)))))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Singleton list of machine containing 101. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define %nginx-machine
  (machine
   (operating-system %system)
   (environment managed-host-environment-type)
   (configuration %nginx-101)))

(list %nginx-machine)
